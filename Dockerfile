FROM node:12

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 5002 5432

CMD [ "npm", "run", "dev" ]