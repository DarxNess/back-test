// Bring in our dependencies
const express = require('express');
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV || 'development'}`
});
console.log("process.env.NODE_ENV ",process.env.NODE_ENV );
if(process.env.NODE_ENV == "development"){
  process.env.URLBACK = "localhost";
  process.env.URLFRONT = "localhost";
  process.env.URLSOCKET = "localhost";
}
var path = require('path');

const apiReadApp = express();

apiReadApp.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});


apiReadApp.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/private/index.html'));
});

let SwaggerConnect = require('swagger-connect');
SwaggerConnect.create({ appRoot: __dirname + "/private" }, function(err, swaggerConnect) {
    if (err) { throw err; }
    apiReadApp.set('trust proxy', 1);
    apiReadApp.set('view engine', 'hjs');
    swaggerConnect.register(apiReadApp);
    apiReadApp.listen(process.env.PORTBACK);
});
module.exports = apiReadApp;
console.log('Listening on backend port ' + process.env.PORTBACK);