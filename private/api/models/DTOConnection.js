const EstandarResponse = require("../controllers/Utils/utils");
const db = require("../database/db");

function SimpleConection(sql, Datos) {
    return new Promise(function (resolve, reject) {

        console.log("antes de query");
        db.query(sql, (err, datos) => {
            console.log("datos", datos);
 
            try {            console.log("err", err);

                if (err) {
                    switch (err.code) {
                        case "ER_DUP_ENTRY":
                            var output = EstandarResponse.StandarResponse(
                                "false",
                                "Error en Ejecución ",
                                500,
                                "Valor duplicado"
                            );
                            break;
                        case "ER_BAD_NULL_ERROR":
                            var output = EstandarResponse.StandarResponse(
                                "false",
                                "Error en Ejecución ",
                                500,
                                "Debé ingresar campos requeridos"
                            );
                            break;
                        default:
                            var output = EstandarResponse.StandarResponse(
                                "false",
                                "Error en Ejecución ",
                                500,
                                err.sqlMessage
                            );
                            break;
                    }

                } else {
                    var output = EstandarResponse.StandarResponse(
                        "true",
                        "Exito en Ejecución",
                        200,
                        datos
                    );
                }
            } catch (error) {
                console.log("ERROR DTO ", error);
                reject(error);
            } finally {
                if (output.statusCode == 500) {
                    console.log("finally DTO ", output);
                }
                resolve(output);
            }
        });

    });
}

module.exports = {
    SimpleConection
}