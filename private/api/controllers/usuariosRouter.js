var usuariosDAO = require('../models/usuariosDAO');
const bcrypt = require('bcrypt');
const saltRounds = 10;
module.exports = {

    setUsuario: async (SystemDataRequest, res) => {
        try {
            SystemDataRequest.body.password = bcrypt.hashSync(SystemDataRequest.body.password, saltRounds);
            var procesos = await usuariosDAO.modelsMethod(SystemDataRequest);
            res.send(procesos);

        } catch (error) {
            var respuesta = {
                success: 'false',
                message: 'Error en Ejecución ',
                statusCode: 500,
                response: error
            }
            console.log("router", error);
            res.send(respuesta)
        }
    },
    updateUsuario: async (SystemDataRequest, res) => {
        try {
            var procesos = await usuariosDAO.modelsMethod(SystemDataRequest);
            res.send(procesos);

        } catch (error) {
            var respuesta = {
                success: 'false',
                message: 'Error en Ejecución ',
                statusCode: 500,
                response: error
            }
            console.log("router", error);
            res.send(respuesta)
        }
    },
    updatePassword: async (SystemDataRequest, res) => {
        try {
            SystemDataRequest.body.new_password = bcrypt.hashSync(SystemDataRequest.body.new_password, saltRounds);
            var procesos = await usuariosDAO.modelsMethod(SystemDataRequest);
            res.send(procesos);

        } catch (error) {
            var respuesta = {
                success: 'false',
                message: 'Error en Ejecución ',
                statusCode: 500,
                response: error
            }
            console.log("router", error);
            res.send(respuesta)
        }
    },
    deleteUsuario: async (SystemDataRequest, res) => {
        try {
            var procesos = await usuariosDAO.modelsMethod(SystemDataRequest);
            res.send(procesos);

        } catch (error) {
            var respuesta = {
                success: 'false',
                message: 'Error en Ejecución ',
                statusCode: 500,
                response: error
            }
            console.log("router", error);
            res.send(respuesta)
        }
    },
    getUsuarios: async (SystemDataRequest, res) => {
        try {
            var procesos = await usuariosDAO.modelsMethod(SystemDataRequest);
            if (procesos.success && procesos.statusCode == 200) {
                var respuesta = {
                    "rowCount":procesos.response.rowCount,
                    "rows": procesos.response.rows
                }
                res.send(respuesta);
            } else {
                procesos.response = "Falla de conexión";
                res.send(procesos);

            }
        } catch (error) {
            var respuesta = {
                success: 'false',
                message: 'Error en Ejecución ',
                statusCode: 500,
                response: error
            }
            console.log("router", error);
            res.send(respuesta)

        }
    },
    getUsuarioByID: async (SystemDataRequest, res) => {
        try {
            var procesos = await usuariosDAO.modelsMethod(SystemDataRequest);
            if (procesos.success && procesos.statusCode == 200) {
                res.send(procesos);
            } else {
                procesos.response = "Falla de conexión";
                res.send(procesos);
            }
        } catch (error) {
            var respuesta = {
                success: 'false',
                message: 'Error en Ejecución ',
                statusCode: 500,
                response: error
            }
            console.log("router", error);
            res.send(respuesta)
        }
    }
}