module.exports = {
  StandarResponse: function(
    success = false,
    message = "no_data_found",
    statusCode = 400,
    response = {}
  ) {
    return {
      success: success,
      message: message,
      statusCode: statusCode,
      response: response
    };
  }

};
