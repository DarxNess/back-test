const { Client } = require('pg')



const { PG_HOST, PG_USER, PG_PASSWORD, PG_DATABASE } = process.env;

const connectionData = {
    host: PG_HOST,
    user: PG_USER,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    port: 5432,
}

const db = new Client(connectionData)

db.connect();

module.exports = db;
