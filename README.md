# Test técnico Nodejs
## Cargo BackEnd - Duración 4 hrs



Para comenzar a usar la aplicación deben realizar los siguientes pasos:

1. Clonar el repositorio.
2. Realizar un npm i.
3. Crear la base de datos.
-- Database: back_test

-- DROP DATABASE back_test;

CREATE DATABASE back_test
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Chile.1252'
    LC_CTYPE = 'Spanish_Chile.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    
4. Crear la tabla 
-- Table: public.users_system

-- DROP TABLE public.users_system;

CREATE TABLE public.users_system
(
    id integer NOT NULL,
    name character varying(100) COLLATE pg_catalog."default",
    email character varying(100) COLLATE pg_catalog."default" NOT NULL,
    password character varying(100) COLLATE pg_catalog."default" NOT NULL,
    status character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT users_system_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.users_system
    OWNER to postgres;
    
5. Cambiar las variables de entorno en el archivo .env.development.
MYSQL_HOST=localhost
MYSQL_USER="postgres"
MYSQL_PASSWORD=your_password
MYSQL_DATABASE=back_test

6. Ejecutar npm run dev.
7. Acceder a la ruta http://localhost:5002

Para acceder a los endpoints utilizaremos los siguientes.

1. Obtener todos los usuarios.
http://localhost:5002/users

2. Obtener un usuario en especifico a traves de su id.
http://localhost:5002/users/{id}

3. Para ingresar un nuevo usuario se debe ingresar a la siguiente URL mediante metodo post y con el siguiente contenido de ejemplo en el body en formato JSON.
http://localhost:5002/users/{id}

{
    "name": "prueba",
    "password": "prueba",
    "email": "prueba@prueba.cl",
    "status": "Prueba",
    "id": 3
    
}

4. Para editar datos se debe ingresar a traves del body en formato JSON la siguiente estructura de datos.

http://localhost:5002/users/{id}

{
    "name": "prueba",
    "password": "prueba",
    "email": "prueba@prueba.cl",
    "status": "Prueba",
    "id": 3
    
}

5. Para eliminar un usuario se debe acceder a la siguiente ruta con el metodo DELETE.

http://localhost:5002/users/{id}



